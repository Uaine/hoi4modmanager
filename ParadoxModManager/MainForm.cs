﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;

namespace ParadoxModManager
{
    public partial class MainForm : Form
    {
        string Dir;
        string HOI4Dir;
        int SelectedProfile;
        public MainForm()
        {
            InitializeComponent();
            listView1.CheckBoxes = true;
            SelectedProfile = 1;

            string DocmsDir = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            Dir = Path.Combine(DocmsDir, "Paradox Interactive", "ModManager");
            HOI4Dir = Path.Combine(DocmsDir, "Paradox Interactive", "Hearts of Iron IV");
            string HOI4ModsDir = Path.Combine(HOI4Dir, "mod");
            string[] HOI4ModsFPs= Directory.GetFiles(HOI4ModsDir, "*.mod",SearchOption.TopDirectoryOnly);

            AppendMods(ref listView1, HOI4ModsFPs);

            if (!Directory.Exists(Dir))
                Directory.CreateDirectory(Dir);
        }
        void AppendMods(ref ListView listview, string[] fps)
        {
            foreach(string fp in fps)
            {
                string name = getName(fp);
                string[] items = { "", name, fp };
                listview.Items.Add(new ListViewItem(items));
            }
        }
        string getName(string fp)
        {
            StreamReader reader = new StreamReader(fp);
            string line;
            while (null != (line = reader.ReadLine()))
            {
                string[] splitLine = line.Split('=');
                if (splitLine[0] == "name")
                    return splitLine[1];
            }
            reader.Dispose();
            return "";
        }

        private void button11_Click(object sender, EventArgs e)
        {
            SaveChecked(SelectedProfile, listView1);
        }

        private void SaveChecked(int profile, ListView listview)
        {
            string fp = Path.Combine(Dir, "profile"+profile.ToString()+".conf");
            FileStream fs = new FileStream(fp, FileMode.Create);
            StreamWriter writer = new StreamWriter(fs);
            for (int i = 0; i < listview.Items.Count; i++)
            {
                if (listview.Items[i].Checked)
                    writer.WriteLine(listview.Items[i].SubItems[2].Text);
            }
            writer.Dispose();
        }
        private void SetChecked(int profile, ListView listview, string dir)
        {
            string fp = Path.Combine(dir, "settings.txt");
            FileStream fs = new FileStream(fp, FileMode.Open);
            StreamReader reader = new StreamReader(fs);
            string line;
            List<string> Lines = new List<string>();
            while (null != (line = reader.ReadLine()))
            {
                Lines.Add(line);
            }
            int startindex = FindStartIndex(Lines);
            int count = FindCurrentNumber(Lines);
            //erase 2 lines;
            for (int i = 0; i < count; i++)
            {
                Lines.RemoveAt(startindex + 1);
            }
            reader.Dispose();
            fs.Dispose();

            //add my lines in
            for (int i = 0; i < listview.Items.Count; i++)
            {
                if (listview.Items[i].Checked)
                {
                    string filename = CutToFilename(listview.Items[i].SubItems[2].Text);
                    string InsertLine = "\"mod/"+filename + "\"";
                    Lines.Insert(startindex+1, InsertLine);
                }
            }

            //save file
            fs = new FileStream(fp, FileMode.Open);
            StreamWriter sw = new StreamWriter(fs);
            foreach (string lineToWrite in Lines)
            {
                sw.WriteLine(lineToWrite);
            }
            sw.Dispose();
            fs.Dispose();
        }
        private string CutToFilename(string input)
        {
            string output;
            int index = input.IndexOf("ugc_");
            output = input.Remove(0, index);
            return output;
        }
        private int FindStartIndex(List<string> Lines)
        {
            for (int i = 0; i < Lines.Count; i++)
            {
                string[] splitLine = Lines[i].Split('=');
                if (splitLine[0] == "last_mods")
                {
                    return i;
                }
            }
            return 0;
        }
        private int FindCurrentNumber(List<string> Lines)
        {
            int output = 0;
            for (int i = 0; i < Lines.Count; i++)
            {
                if (Lines[i].Contains("mod/ugc"))
                {
                    output += 1;
                }
            }
            return output;
        }
        private void button12_Click(object sender, EventArgs e)
        {
            string[] names = GetActivatedNames(HOI4Dir);
            int[] indexes = findMatchingNames(names, ref listView1);
            for (int i = 0; i < listView1.Items.Count; i++)
                listView1.Items[i].Checked = false;
            foreach(int index in indexes)
                listView1.Items[index].Checked = true;
        }
        private int[] findMatchingNames(string[] names, ref ListView listview)
        {
            List<int> Indexes = new List<int>();
            foreach (string filename in names)
            {
                foreach (ListViewItem item in listview.Items)
                {
                    if (item.SubItems[2].Text.Contains(filename))
                    {
                        Indexes.Add(item.Index);
                    }
                }
            }
            return Indexes.ToArray();
        }
        private string[] GetActivatedNames(string dir)
        {
            List<string> nameslist = new List<string>();
            string fp = Path.Combine(dir, "settings.txt");
            StreamReader reader = new StreamReader(fp);
            string line;
            while (null != (line = reader.ReadLine()))
            {
                string[] splitLine = line.Split('=');
                if (splitLine[0] == "last_mods")
                    break;
            }
            while (true)
            {
                line = reader.ReadLine();
                if (line.Contains("mod/"))
                {
                    string[] splitline = line.Split('/');
                    splitline[1] = splitline[1].Remove(splitline[1].Length - 1, 1);
                    nameslist.Add(splitline[1]);
                }
                else break;
            }
            reader.Dispose();
            return nameslist.ToArray();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            SaveChecked(SelectedProfile, listView1);
            SetChecked(SelectedProfile, listView1, HOI4Dir);
        }
    }
}
